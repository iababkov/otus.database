﻿using System;
using System.Collections;

namespace OtusDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.SetBufferSize(50, 50);
            Console.SetWindowSize(50, 50);

            var postGreService = new PostGreService("localHost", "postgres", "Pa$$word", "OtusTutorial");

            OutPutRecords(postGreService.GetTableRecordStringValues("public", "Person"));

            OutPutRecords(postGreService.GetTableRecordStringValues("public", "PersonDocument"));

            OutPutRecords(postGreService.GetTableRecordStringValues("public", "BankAccount"));

            int numberOfRowsInserted = postGreService.InsertTableRecord("public", "PersonDocument", "3", "International Passport", "3030-000333", "03.03.2021");

            if (numberOfRowsInserted > 0)
            {
                Console.WriteLine($"Rows inserted: {numberOfRowsInserted}");
            }
            else
            {
                Console.WriteLine("Error on Insert command:");
                Console.WriteLine(postGreService.ErrorMessage);
            }
        }

        static void OutPutRecords(IEnumerable recordList)
        {
            foreach (string stringValue in recordList)
            {
                Console.WriteLine(stringValue);
            }

            Console.ReadLine();
        }
    }
}
