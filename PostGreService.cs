﻿using System;
using System.Collections;
using Npgsql;

namespace OtusDB
{
    class PostGreService
    {
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DataBase { get; set; }
        public string ErrorMessage { get { return _errorMessage; } }
        private string _errorMessage;

        public PostGreService(string host, string userName, string password, string dataBase)
        {
            Host = host;
            UserName = userName;
            Password = password;
            DataBase = dataBase;
        }

        public IEnumerable GetTableRecordStringValues(string schema, string tableName)
        {
            using var connection = new NpgsqlConnection($"Host={Host}; UserName={UserName}; Password={Password}; DataBase={DataBase}");
            connection.Open();

            using var command = new NpgsqlCommand($"SELECT * FROM {schema}.\"{tableName}\"", connection);
            using var reader = command.ExecuteReader();
            int recordCounter = 0;

            while (reader.Read())
            {
                yield return $"-----{++recordCounter}-----";

                for (ushort fieldNum = 0; fieldNum < reader.FieldCount; fieldNum++)
                {
                    yield return reader.GetValue(fieldNum).ToString();
                }
            }
        }

        public int InsertTableRecord(string schema, string tableName, params string[] columnValues)
        {
            int numberOfRowsAffected = 0;

            try
            {
                string insertValues = "";

                foreach (string value in columnValues)
                {
                    insertValues += (insertValues == "" ? "" : ',') + $"\'{value}\'";
                }

                using var connection = new NpgsqlConnection($"Host={Host}; UserName={UserName}; Password={Password}; DataBase={DataBase}");
                connection.Open();

                using var command = new NpgsqlCommand($"INSERT INTO {schema}.\"{tableName}\" VALUES ({insertValues})", connection);

                numberOfRowsAffected = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
            }

            return numberOfRowsAffected;
        }
    }
}
